-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2016 at 06:20 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `wcifx_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `wcifx_accountmt4`
--

CREATE TABLE IF NOT EXISTS `wcifx_accountmt4` (
  `account_id` bigint(15) NOT NULL,
  `member_id` int(11) NOT NULL,
  `accountmt4category_id` int(5) NOT NULL,
  `date_created` datetime NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wcifx_accountmt4category`
--

CREATE TABLE IF NOT EXISTS `wcifx_accountmt4category` (
  `accountmt4category_id` int(5) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(50) NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `wcifx_accountmt4category`
--

INSERT INTO `wcifx_accountmt4category` (`accountmt4category_id`, `name`, `type`, `status`) VALUES
(1, 'Base Account', 'base_account', 1),
(2, 'Pamm Manager', 'pamm_manager', 1),
(3, 'Investor', 'pamm_investor', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wcifx_member`
--

CREATE TABLE IF NOT EXISTS `wcifx_member` (
  `member_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `date_created` datetime NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `gender` varchar(1) NOT NULL,
  `date_born` date NOT NULL,
  `country` varchar(30) NOT NULL,
  `province` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `avatar` varchar(200) NOT NULL,
  `upline` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `wcifx_webtraffic`
--

CREATE TABLE IF NOT EXISTS `wcifx_webtraffic` (
  `id` bigint(15) NOT NULL,
  `date` date NOT NULL,
  `ip_address` varchar(20) NOT NULL,
  `user_agent` text NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wcifx_accountmt4`
--
ALTER TABLE `wcifx_accountmt4`
  ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `wcifx_accountmt4category`
--
ALTER TABLE `wcifx_accountmt4category`
  ADD PRIMARY KEY (`accountmt4category_id`);

--
-- Indexes for table `wcifx_member`
--
ALTER TABLE `wcifx_member`
  ADD PRIMARY KEY (`member_id`);

--
-- Indexes for table `wcifx_webtraffic`
--
ALTER TABLE `wcifx_webtraffic`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wcifx_accountmt4`
--
ALTER TABLE `wcifx_accountmt4`
  MODIFY `account_id` bigint(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wcifx_accountmt4category`
--
ALTER TABLE `wcifx_accountmt4category`
  MODIFY `accountmt4category_id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `wcifx_member`
--
ALTER TABLE `wcifx_member`
  MODIFY `member_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `wcifx_webtraffic`
--
ALTER TABLE `wcifx_webtraffic`
  MODIFY `id` bigint(15) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
