soal test
	-buat core untuk codeigniter nya
	-buat function untuk register & login member
	-buat function untuk visitor counting (nanti akan digunakan untuk analytic visitor)
ketentuan :
	-register dan login menggunakan ajax
	-silahkan implementasikan juga untuk member login nya (is_member , is_login , dll) , karena nanti terdapat 2 user (admin dan member) , untuk admin nanti 
	-visitor counting memakai hook Codeigniter
	-untuk hash password gunakan function password_hash dan password_verify
flow
	1.untuk register itu nanti setelah klik 'register' akan add di table 'member' & 'account'
		a.di table 'member' nanti semua required kecuali field avatar(ini nanti disi default dulu , soalnya edit avatar nya di halaman profile nanti) , upline(ini nanti untuk program partnership , di isi member_id)
		b.	-accountmt4category_id di dapatkan dari table accountmt4category where type='base_account'
			-di table account , semua required kecuali username(ini untuk username trading , tapi karena ini 'base_account' username nya disamakan dgn username member) , password(ini nanti untuk password trading) , avatar(sama seperti avatar di member , di isi default dulu , nanti bisa di edit di profile account)
	2.untuk login nanti pakai username/email & password dari table "member"
	3.untuk visitor counting algoritma nya adalah dengan mencocok kan ip address dan user agent visitor , ini daily ya jadi checking algoritma nya per hari
penilaian 
	-Kecepatan (25%)
	-Kerapihan (25%)
	-Security (25%)
	-Struktur file(Model , View , Controller) (25%)

#note table : 
	-member
	-accountmt4
	-accountmt4category
	-webtraffic